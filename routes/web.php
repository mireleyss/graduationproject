<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ChampionController;
use App\Http\Controllers\MainPageController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

//Route::get('/categories', [CategoryController::class, 'index']);
//Route::get('/categories/create', [CategoryController::class, 'create']);
//Route::post('/categories', [CategoryController::class, 'store'])->name('category.store');
Route::get('/', [MainPageController::class, 'index'])
    ->name('monster-truck.index');
//Route::get('/', [ArticleController::class, 'index'])
//    ->name('monster-truck.index');
//
//Route::get('/', [ChampionController::class, 'index'])
//    ->name('monster-truck.index');
Route::get('/all_articles', [ArticleController::class, 'index'])
    ->name('articles.index');

Route::get('/articles/{article}', [ArticleController::class, 'show'])
    ->name('articles.show');

Route::get('/all_champions', [ChampionController::class, 'index'])
    ->name('champions.index');

Route::get('/all_champions/{champion}', [ChampionController::class, 'show'])
    ->name('champions.show');

Route::get('/login', [AuthController::class, 'index'])
    ->name('login');

Route::post('/login', [AuthController::class, 'loginUser'])
    ->name('loginUser');


Route::get('/register', [AuthController::class, 'registration'])
    ->name('registration');

Route::post('/register', [AuthController::class, 'store'])
    ->name('user.store');

Route::get('/profile', [AuthController::class, 'profile'])
    ->name('user.profile');

Route::post('/profile', [AuthController::class, 'logout'])
    ->name('user.logout');

//admin
Route::prefix('/admin_panel')->middleware(['auth', 'isAdmin'])->group(function () {
    Route::get('/', [AdminController::class, 'adminPanel'])
        ->name('admin_panel');
    Route::get('/champions', [AdminController::class, 'showChampions'])
        ->name('admin.champions_list');
    Route::get('/champions/create', [AdminController::class, 'createChampions'])
        ->name('admin.create.create_champions');
    Route::post('/champions/create', [AdminController::class, 'storeChampions'])
        ->name('admin.store_champions');
    Route::delete('/champions/{champion}', [AdminController::class, 'destroyChampions'])
        ->name('admin.destroyChampions');
    Route::get('/champions/{champion}/edit', [AdminController::class, 'editChampions'])
        ->name('admin.edit.champions');
    Route::put('/champions/{champion}', [AdminController::class, 'updateChampions'])
        ->name('admin.updateChampions');

    //cars
    Route::get('/cars', [AdminController::class, 'showCars'])
        ->name('admin.cars_list');
    Route::get('/cars/create', [AdminController::class, 'createCars'])
        ->name('admin.create.cars');
    Route::post('/cars/create', [AdminController::class, 'storeCars'])
        ->name('admin.store_cars');
    Route::delete('/cars/{car}', [AdminController::class, 'destroyCars'])
        ->name('admin.destroyCars');
    Route::get('/cars/{car}/edit', [AdminController::class, 'editCars'])
        ->name('admin.edit.cars');
    Route::put('/cars/{car}', [AdminController::class, 'updateCars'])
        ->name('admin.updateCars');

    //articles
    Route::prefix('/articles')->group(function () {
        Route::get('/', [AdminController::class, 'showArticles'])
            ->name('admin.articles_list');
        Route::get('/create', [AdminController::class, 'createArticles'])
            ->name('admin.create.articles');
        Route::post('/create', [AdminController::class, 'storeArticles'])
            ->name('admin.store_articles');
        Route::delete('/{article}', [AdminController::class, 'destroyArticles'])
            ->name('admin.destroyArticles');
        Route::get('/{article}/edit', [AdminController::class, 'editArticles'])
            ->name('admin.edit.articles');
        Route::put('/{article}', [AdminController::class, 'updateArticles'])
            ->name('admin.updateArticles');
    });


    //slider
    Route::prefix('/slider')->group(Function () {
        Route::get('/', [AdminController::class, 'showSlides'])
            ->name('admin.slider');
        Route::get('/create', [AdminController::class, 'createSlides'])
            ->name('admin.create.slider');
        Route::post('/create', [AdminController::class, 'storeSlides'])
            ->name('admin.store_slides');
        Route::delete('/{slider}', [AdminController::class, 'destroySlides'])
            ->name('admin.destroySlides');
        Route::get('/{slider}/edit', [AdminController::class, 'editSlides'])
            ->name('admin.edit.slides');
        Route::put('/{slider}', [AdminController::class, 'updateSlider'])
            ->name('admin.update_slider');
    });

    //users
    Route::prefix('/users')->group(function () {
        Route::get('/', [AdminController::class, 'showUsers'])
            ->name('admin.users_list');
        Route::get('/create', [AdminController::class, 'createAdmins'])
            ->name('admin.create.admins');
        Route::post('/create', [AdminController::class, 'storeAdmins'])
            ->name('admin.store_admins');
        Route::delete('/{user}', [AdminController::class, 'destroyUsers'])
            ->name('admin.destroyUsers');
    });
});

