<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {

        $rules = [
            'header' => 'required|string|min:3|max:256',
            'content' => 'required|string|min:10|max:10000',
            'article_image' => 'nullable',
        ];

        if (request()->hasFile('article_image')
            && request()->file('article_image')->isValid()) {
            $rules['article_image'] = 'image|mimes:jpg,png,jpeg|max:2048';
        }

        return $rules;
    }

    public function messages(): array
    {
        return [
            'header.required' => 'A header is required',
            'header.min' => 'A header is too small',
            'header.max' => 'A header is too large',
            'content.required' => 'A content is required',
            'content.min' => 'A content is too small',
            'content.max' => 'A content is too large',
            'article_image.mimes' => 'Unsupported file format',
            'article_image.max' => 'Image is too large',
        ];
    }
}
