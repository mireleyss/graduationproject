<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SliderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $rules = [
            'image' => 'required|image|mimes:jpg,png,jpeg|max:2048',
        ];

//        if (request()->hasFile('image')
//            && request()->file('image')->isValid()) {
//
//        }

        return $rules;
    }

    public function messages(): array
    {
        return [
            'image.required' => 'Image is required',
            'image.mimes' => 'Unsupported file format',
            'image.max' => 'Image is too large',
        ];
    }
}
