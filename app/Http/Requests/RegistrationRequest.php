<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'username' => 'required|min:3|max:36',
            'email' => 'required|unique:users|email|min:6|max:128',
            'password' => 'required|min:6|max:64',
        ];
    }

    public function messages(): array
    {
        return [
            'username.min' => 'Username is too small',
            'email.unique' => 'User with this email already exists',
            'password.min' => 'password is too small',
        ];
    }
}
