<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|unique:cars|min:3|max:64',
            'max_speed' => 'nullable|numeric|min:1|max:300',
            'horsepower' => 'nullable|numeric|min:1|max:5000',
            'engine_capacity' => 'nullable|numeric|min:1|max:99.99',
            'release_year' => 'nullable|numeric|min:1900|max:2099',
        ];
    }

    public function messages(): array
    {
        return [
            'name.required' => 'Name is required',
            'name.min' => 'Name is too small',
            'name.max' => 'Name is too large',
            'name.unique' => 'Name must be unique',
            'max_speed.min' => 'Value is too small',
            'max_speed.max' => 'Value is too large',
            'max_speed.numeric' => 'Value must be a number',
            'horsepower.min' => 'Value is too small',
            'horsepower.max' => 'Value is too large',
            'horsepower.numeric' => 'Value must be a number',
            'engine_capacity.min' => 'Value is too small',
            'engine_capacity.max' => 'Value is too large',
            'engine_capacity.numeric' => 'Value must be a number',
            'release_year.min' => 'Value is too small',
            'release_year.max' => 'Value is too large',
            'release_year.numeric' => 'Value must be a number',
        ];
    }
}
