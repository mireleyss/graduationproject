<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChampionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $rules = [
            'name' => 'required|min:3|max:128',
            'info' => 'nullable|max:10000',
            'champion_portrait' => 'nullable',
        ];

        if (request()->hasFile('champion_portrait')
            && request()->file('champion_portrait')->isValid()) {
            $rules['champion_portrait'] = 'image|mimes:jpg,png,jpeg|max:2048';
        }

        return $rules;
    }

    public function messages(): array
    {
        return [
            'name.required' => 'Name is required',
            'name.min' => 'Name is too small',
            'name.max' => 'Name is too large',
            'info.required' => 'A content is required',
            'info.min' => 'A content is too small',
            'info.max' => 'A content is too large',
            'champion_portrait.mimes' => 'Unsupported file format',
            'champion_portrait.max' => 'Image is too large',
        ];
    }
}
