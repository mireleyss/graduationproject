<?php

namespace App\Http\Middleware;

use Closure;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IsAdmin
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::check()){
            return redirect()->route('login');
        }

        if(Auth::user()->isAdmin == 1){
            return $next($request);
        }

//        if(Auth::user()){
//            return $next($request);
//        }

        return redirect()->back()->with('unauthorised', 'You are
                  unauthorised to access this page');
    }
}
