<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthRequest;
use App\Http\Requests\RegistrationRequest;
use App\Models\User;
use App\Services\UserService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\View\View;

class AuthController extends Controller
{
    private UserService $userService;

    public function __construct(
        UserService  $userService)
    {
        $this->userService = $userService;
    }
    public function index(): View|RedirectResponse
    {
        if (Auth::user()) return redirect('/');

        return view('auth.login');
    }

    public function loginUser(AuthRequest $request): RedirectResponse
    {
        $credentials = $request->validated();

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            if (Auth::user()->isAdmin) {
                return redirect()->intended('/admin_panel');
            }

            return redirect()->intended('/');
        }

        return back()->withErrors([
            'email' => 'The provided credentials don\'t match our records',
        ])->onlyInput('email');
    }

    public function registration(): View|RedirectResponse
    {
        if (Auth::user()) return redirect('/');

        return view('auth.registration');
    }

    public function store(RegistrationRequest $request): RedirectResponse
    {
        $this->userService->store($request->validated());

        return redirect()->route("monster-truck.index");
    }

    public function profile(): View
    {
        if (Auth::check()) {
            $required_fields = ['username', 'email'];
            $user_data = Auth::user()->only($required_fields);

            return view('user.profile', compact('user_data'));
        }
    }

    public function logout(Request $request): RedirectResponse
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect('/');
    }
}
