<?php

namespace App\Http\Controllers;

use App\Http\Requests\ArticleRequest;
use App\Http\Requests\CarRequest;
use App\Http\Requests\ChampionRequest;
use App\Http\Requests\RegistrationRequest;
use App\Http\Requests\SliderRequest;
use App\Models\Article;
use App\Models\Car;
use App\Models\Champion;
use App\Models\Slider;
use App\Models\User;
use App\Services\ArticleService;
use App\Services\CarService;
use App\Services\ChampionService;
use App\Services\SliderService;
use App\Services\UserService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;

class AdminController extends Controller
{
//    public string $champions_folder = 'storage/champions_portraits';
    private ChampionService $championService;
    private CarService $carService;
    private ArticleService $articleService;
    private SliderService $sliderService;
    private UserService $userService;

    public function __construct(
        SliderService   $sliderService,
        ChampionService $championService,
        CarService      $carService,
        ArticleService  $articleService,
        UserService $userService,
    )
    {
        $this->sliderService = $sliderService;
        $this->championService = $championService;
        $this->carService = $carService;
        $this->articleService = $articleService;
        $this->userService = $userService;
    }

    public function adminPanel(): View
    {
        return view('admin.admin_panel');
    }

    //slider

    public function showSlides(): View
    {
        define('SLIDES_ON_PAGE', 10);
        $sliders = $this->sliderService->paginate(SLIDES_ON_PAGE);

        return view('admin.slider', compact('sliders'));
    }

    public function createSlides(): View
    {
        return view('admin.create.slider');
    }

    public function storeSlides(SliderRequest $request): RedirectResponse
    {
        $this->sliderService->store($request->validated());
        return redirect()->route("admin.slider");
    }

    public function updateSlider(SliderRequest $request, Slider $slider): RedirectResponse
    {
        $this->sliderService->update($request->validated(), $slider);
        return redirect()->route("admin.slider");
    }

    public function editSlides(Slider $slider): View
    {
        return view('admin.edit.slides', compact('slider'));
    }

    public function destroySlides(Slider $slider): RedirectResponse
    {
        $this->sliderService->destroy($slider);
        return redirect()->route('admin.slider');
    }


    public function showChampions(): View
    {
        define("CHAMPIONS_ON_PAGE", 10);
        $champions = $this->championService->getChampionsOnAdmin(CHAMPIONS_ON_PAGE);

        return view('admin.champions_list', compact('champions'));
    }

    public function showCars(): View
    {
        define("CARS_ON_PAGE", 10);
        $cars = $this->carService->paginate(CARS_ON_PAGE);

        return view('admin.cars_list', compact('cars'));
    }

    public function showArticles(): View
    {
        define("ARTICLES_ON_PAGE", 10);
        $articles = $this->articleService->getArticlesOnAdmin(ARTICLES_ON_PAGE);
        return view('admin.articles_list', compact('articles'));
    }

    public function createChampions(): View
    {
        $cars = $this->carService->all();
        return view('admin.create.create_champions', compact('cars'));
    }

    public function storeChampions(ChampionRequest $request): RedirectResponse
    {
        $this->championService->store($request->validated());
        return redirect()->route("admin.champions_list");
    }

    public function editChampions(Champion $champion): View
    {
        $cars = $this->carService->all();
        return view('admin.edit.champions', compact(['champion', 'cars']));
    }

    public function updateChampions(ChampionRequest $request, Champion $champion): RedirectResponse
    {
        $this->championService->update($request->validated(), $champion);
        return redirect()->route('admin.champions_list');
    }

    public function destroyChampions(Champion $champion): RedirectResponse
    {
        $this->championService->destroy($champion);
        return redirect()->route('admin.champions_list');
    }

    public function createCars(): View
    {
        return view('admin.create.cars');
    }

    public function storeCars(CarRequest $request): RedirectResponse
    {
        $this->carService->store($request->validated());
        return redirect()->route('admin.cars_list');
    }

    public function editCars(Car $car): View
    {
        return view('admin.edit.cars', compact('car'));
    }

    public function updateCars(CarRequest $request, Car $car): RedirectResponse
    {
        $this->carService->update($request->validated(), $car);
        return redirect()->route('admin.cars_list');
    }

    public function destroyCars(Car $car): RedirectResponse
    {
        $this->carService->destroy($car);
        return redirect()->route('admin.cars_list');
    }


    // articles
    public function createArticles(): View
    {
        return view('admin.create.articles');
    }

    public function storeArticles(ArticleRequest $request): RedirectResponse
    {
        Log::info('aaa', [$request['content']]);
        $this->articleService->store($request->validated());

        return redirect()->route("admin.articles_list");
    }

    public function editArticles(Article $article): View
    {
        return view('admin.edit.articles', compact('article'));
    }

    public function updateArticles(ArticleRequest $request, Article $article): RedirectResponse
    {
        $this->articleService->update($request->validated(), $article);

        return redirect()->route("admin.articles_list");
    }

    public function destroyArticles(Article $article): RedirectResponse
    {
        $this->articleService->destroy($article);
        return redirect()->route('admin.articles_list');
    }


    //users
    public function showUsers(): View
    {
        define("USERS_ON_PAGE", 10);
        $users = $this->userService->paginate(USERS_ON_PAGE);
        return view('admin.users_list', compact('users'));
    }

    public function createAdmins(): View
    {
        return view('admin.create.admins');
    }

    public function storeAdmins(RegistrationRequest $request): RedirectResponse
    {
        $this->userService->storeAdmins($request->validated());
        return redirect()->route("admin.users_list");
    }

    public function destroyUsers(User $user): RedirectResponse
    {
        $this->userService->destroy($user);
        return redirect()->route('admin.users_list');
    }
}
