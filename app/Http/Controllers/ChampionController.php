<?php

namespace App\Http\Controllers;

use App\Models\Car;
use App\Models\Champion;
use App\Services\CarService;
use App\Services\ChampionService;
use Illuminate\View\View;

class ChampionController extends Controller
{
    private ChampionService $championService;
    private CarService $carService;

    public function __construct(ChampionService $championService, CarService $carService)
    {
        $this->championService = $championService;
        $this->carService = $carService;
    }

    public function index(): View
    {
        define('CHAMPIONS_ON_PAGE', 12);
        $champions = $this->championService->paginate(CHAMPIONS_ON_PAGE);
        return view('champions.index', compact('champions'));
    }

    public function show(Champion $champion): View
    {
        if($champion->car) {
            $columns = [
                'max_speed'       => 'Максимальная скорость',
                'horsepower'      => 'Лошадиных сил',
                'engine_capacity' => 'Объем двигателя',
                'release_year'    => 'Год выпуска',
            ];

            $car_characteristics = $this->carService->getCarData($champion->car->id);

            return view('champions.show', compact(['champion', 'car_characteristics', 'columns']));
        }

        return view('champions.show', compact(['champion']));
    }

    public function create(): View
    {
        return view('champions.create');
    }
}
