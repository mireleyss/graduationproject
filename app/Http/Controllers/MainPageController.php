<?php

namespace App\Http\Controllers;

use App\Services\ArticleService;
use App\Services\ChampionService;
use App\Services\SliderService;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\View\View;

class MainPageController extends Controller
{
    private ChampionService $championService;
    private ArticleService $articleService;
    private SliderService $sliderService;

    public function __construct(
        ChampionService $championService,
        ArticleService $articleService,
        SliderService $sliderService)
    {
        $this->championService = $championService;
        $this->articleService = $articleService;
        $this->sliderService = $sliderService;
    }

    public function index(): View
    {
        $champions = $this->championService->getFirstFourChampions();
        $articles = $this->articleService->getFirstThreeArticles();
        $slider = $this->sliderService->all();

        return view('monster-truck.index', compact(['slider', 'champions', 'articles']));
    }
}

;
