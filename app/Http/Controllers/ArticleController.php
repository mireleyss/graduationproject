<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Services\ArticleService;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ArticleController extends Controller
{
    private ArticleService $articleService;

    public function __construct(ArticleService $articleService)
    {
        $this->articleService = $articleService;
    }

    public function index(): View
    {
        define('ARTICLES_ON_PAGE', 10);
        $articles = $this->articleService->paginate(ARTICLES_ON_PAGE);
        return view('articles.index', compact('articles'));
    }

    public function show(Article $article): View
    {
        $recommended_articles = $this->articleService->getRecommendedArticles($article->id);
        return view('articles.show', compact(['article', 'recommended_articles']));
    }
}
