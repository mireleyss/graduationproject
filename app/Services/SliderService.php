<?php

namespace App\Services;

use App\Models\Slider;
use App\Repositories\SliderRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Storage;

class SliderService
{
    protected SliderRepository $sliderRepository;

    public function __construct(SliderRepository $sliderRepository)
    {
        $this->sliderRepository = $sliderRepository;
    }

    public function all(): Collection
    {
        $slider = $this->sliderRepository->all();

        return $slider->filter(function ($item) {
            return (($item->image) && (Storage::exists('slider_images/' . $item->image)));
        });
    }

    public function store(array $data)
    {
        if (key_exists('image', $data)) {
            $data = $this->putImageInStorage($data);
        }

        return $this->sliderRepository->store($data);
    }

    public function putImageInStorage(array $data): array
    {
        $file = $data['image'];
        $filename = time() . '.' . $file->getClientOriginalExtension();
        Storage::putFileAs('slider_images', $file, $filename);

        $data['image'] = $filename;

        return $data;
    }

    public function update(array $data, Slider $slider): bool
    {
        if (key_exists('image', $data)) {
            $data = $this->updateImage($data, $slider);
        }

        return $this->sliderRepository->update($data, $slider);
    }

    public function updateImage(array $data, Slider $slider): array
    {

        $data = $this->putImageInStorage($data);

        $previous_image = $slider->image;
        $path = 'slider_images/' . $previous_image;

        if (Storage::exists($path)) {
            Storage::delete($path);
        }

        return $data;
    }

    public function deleteImageFromStorage($image): void
    {
        $path = 'slider_images/' . $image;

        if (Storage::exists($path)) {
            Storage::delete($path);
        }
    }

    public function destroy(Slider $slider): ?bool
    {
        if ($slider->image) $this->deleteImageFromStorage($slider->image);
        return $this->sliderRepository->destroy($slider);
    }

    public function paginate(int $items_on_page)
    {
        return $this->sliderRepository->paginate($items_on_page);
    }
}
