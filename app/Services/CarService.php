<?php

namespace App\Services;
use App\Models\Car;
use App\Repositories\CarRepository;
use Illuminate\Database\Eloquent\Collection;

class CarService
{
    protected CarRepository $carRepository;
    public function __construct(CarRepository $carRepository)
    {
        $this->carRepository = $carRepository;
    }

    public function all(): Collection
    {
        return $this->carRepository->all();
    }

    public function paginate(int $items_on_page) {
        return $this->carRepository->paginate($items_on_page);
    }

    public function store(array $data)
    {
        return $this->carRepository->store($data);
    }

    public function update(array $data, Car $car): bool
    {
        return $this->carRepository->update($data, $car);
    }

    public function destroy(Car $car): ?bool
    {
        return $this->carRepository->destroy($car);
    }

    public function getCarData(int $id) {
        return $this->carRepository->getCarData($id);
    }
}
