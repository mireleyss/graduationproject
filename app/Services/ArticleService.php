<?php

namespace App\Services;
use App\Models\Article;
use App\Repositories\ArticleRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ArticleService
{
    protected ArticleRepository $articleRepository;
    public function __construct(ArticleRepository $articleRepository)
    {
        $this->articleRepository = $articleRepository;
    }

    public function all(): Collection
    {
        return $this->articleRepository->all();
    }

    public function getFirstThreeArticles() {
        $articles = $this->articleRepository->getFirstThreeArticles();

        return $this->limitHeaderSize($articles);
    }

    public function getRequiredColumns(array $columns) {
        return $this->articleRepository->getRequiredColumns($columns);
    }

    public function getArticlesOnAdmin(int $items_on_page) {
        $columns = ['id', 'header', 'content', 'article_image', 'publication_date'];

        $articles = $this->getRequiredColumns($columns)->paginate($items_on_page);
        return $this->limitData($articles);
    }

    public function getRecommendedArticles(int $id) {
        return $this->articleRepository->getRecommendedArticles($id);
    }

    public function paginate(int $items_on_page) {
        $articles = $this->articleRepository->paginate($items_on_page);

        return $this->limitHeaderSize($articles);
    }

    public function update(array $data, Article $article): bool
    {
        if (key_exists('article_image', $data)) {
            $data = $this->updateImage($data, $article);
        }

        return $this->articleRepository->update($data, $article);
    }

    public function store(array $data)
    {
        $data['publication_date'] = now();

        if (key_exists('article_image', $data)) {
            $data = $this->putImageInStorage($data);
        }

        return $this->articleRepository->store($data);
    }

    public function destroy(Article $article): ?bool
    {
        if ($article->article_image) $this->deleteImageFromStorage($article->article_image);
        return $this->articleRepository->destroy($article);
    }

    public function limitHeaderSize($articles) {
        foreach ($articles as $article) {
            $article->header = Str::limit($article->header, 36, $end='...');
        }

        return $articles;
    }

    public function limitContentSize($articles) {
        foreach ($articles as $article) {
            $article->content = Str::limit($article->content, 54, $end='...');
        }

        return $articles;
    }

    public function limitData($articles) {
        $articles = $this->limitHeaderSize($articles);
        return $this->limitContentSize($articles);
    }

    public function updateImage(array $data, Article $article): array
    {

        $data = $this->putImageInStorage($data);

        $previous_image = $article->article_image;
        $path = 'articles_images/' . $previous_image;

        if (Storage::exists($path)) {
            Storage::delete($path);
        }

        return $data;
    }

    public function deleteImageFromStorage($image): void
    {
        $path = 'articles_images/' . $image;

        if (Storage::exists($path)) {
            Storage::delete($path);
        }
    }

    public function putImageInStorage(array $data): array
    {
        $file = $data['article_image'];
        $filename = time() . '.' . $file->getClientOriginalExtension();
        Storage::putFileAs('articles_images', $file, $filename);

        $data['article_image'] = $filename;

        return $data;
    }
}
