<?php

namespace App\Services;

use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Storage;

class UserService
{
    protected UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

//    public function all(): Collection
//    {
//        $slider = $this->sliderRepository->all();
//
//        return $slider->filter(function ($item) {
//            return (($item->image) && (Storage::exists('slider_images/' . $item->image)));
//        });
//    }

    public function store(array $data)
    {
        return $this->userRepository->store($data);
    }

    public function storeAdmins(array $data)
    {
        $data['isAdmin'] = true;
        return $this->userRepository->store($data);
    }

    public function paginate(int $items_on_page) {
        return $this->userRepository->paginate($items_on_page);
    }

    public function destroy(User $user) {
        return $this->destroy($user);
    }
}
