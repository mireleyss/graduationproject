<?php

namespace App\Services;

use App\Models\Champion;
use App\Repositories\ChampionRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ChampionService
{
    protected ChampionRepository $championRepository;

    public function __construct(ChampionRepository $championRepository)
    {
        $this->championRepository = $championRepository;
    }

    public function all(): Collection
    {
        return $this->championRepository->all();
    }

    public function getFirstFourChampions()
    {
        return $this->championRepository->getFirstFourChampions();
    }

    public function getRequiredColumns(array $columns) {
        return $this->championRepository->getRequiredColumns($columns);
    }

    public function getChampionsOnAdmin(int $items_on_page) {
//        $columns = ['id', 'name', 'info', 'champion_portrait', 'car_id'];
//        $champions = $this->getRequiredColumns($columns)->paginate($items_on_page);
        $champions = $this->paginate($items_on_page);
        return $this->limitInfoSize($champions);
    }

    public function limitInfoSize($champions) {
        foreach ($champions as $champion) {
            $champion->info = Str::limit($champion->info, 54, $end='...');
        }

        return $champions;
    }

    public function paginate(int $items_on_page) {
        return $this->championRepository->paginate($items_on_page);
    }

    public function update(array $data, Champion $champion): bool
    {
        if (key_exists('champion_portrait', $data)) {
            $data = $this->updateImage($data, $champion);
        }

        return $this->championRepository->update($data, $champion);
    }

    public function store(array $request)
    {
        if (key_exists('champion_portrait', $request)) {
            $request = $this->putImageInStorage($request);
        }

        return $this->championRepository->store($request);
    }

    public function destroy(Champion $champion): ?bool
    {
        if ($champion->champion_portrait) $this->deleteImageFromStorage($champion->champion_portrait);
        return $this->championRepository->destroy($champion);
    }

    public function updateImage(array $data, Champion $champion): array
    {

        $data = $this->putImageInStorage($data);

        $previous_portrait = $champion->champion_portrait;
        $path = 'champions_portraits/' . $previous_portrait;

        if (Storage::exists($path)) {
            Storage::delete($path);
        }

        return $data;
    }


    public function deleteImageFromStorage($image) {
        $path = 'champions_portraits/' . $image;

        if (Storage::exists($path)) {
            Storage::delete($path);
        }
    }

    public function putImageInStorage(array $data): array
    {
        $file = $data['champion_portrait'];
        $filename = time() . '.' . $file->getClientOriginalExtension();
        Storage::putFileAs('champions_portraits', $file, $filename);

        $data['champion_portrait'] = $filename;

        return $data;
    }
}
