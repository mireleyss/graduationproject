<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Database\Eloquent\Collection;

class UserRepository
{
    public function all(): Collection
    {
        return User::all();
    }

    public function store(array $data) {
        return User::create($data);
    }

//    public function update(array $data, Slider $slider): bool
//    {
//        return $slider->update($data);
//    }
//
    public function destroy(User $user): ?bool
    {
        return $user->delete();
    }

    public function paginate(int $items_on_page) {
        return User::paginate($items_on_page);
    }
}
