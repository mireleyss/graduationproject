<?php

namespace App\Repositories;
use App\Models\Car;
use Illuminate\Database\Eloquent\Collection;

class CarRepository
{
    public function all(): Collection
    {
        return Car::all();
    }

    public function paginate(int $items_on_page) {
        return Car::paginate($items_on_page);
    }

    public function store(array $data)
    {
        return Car::create($data);
    }

    public function update(array $data, Car $car): bool
    {
        return $car->update($data);
    }

    public function destroy(Car $car): ?bool
    {
        return $car->delete();
    }

    public function getCarData(int $id) {
        return Car::where('id', $id)
            ->select(
                'max_speed',
                'horsepower',
                'engine_capacity',
                'release_year')
            ->first();
    }

}
