<?php

namespace App\Repositories;

use App\Models\Champion;
use Illuminate\Database\Eloquent\Collection;

class ChampionRepository
{
    public function all(): Collection
    {
        return Champion::all();
    }

    public function getFirstFourChampions()
    {
        return Champion::take(4)->get();
    }

    public function getRequiredColumns(array $columns) {
        return Champion::select($columns);
    }

    public function paginate(int $items_on_page) {
        return Champion::paginate($items_on_page);
    }

    public function update(array $data, Champion $champion): bool
    {
        return $champion->update($data);
    }

    public function store(array $data)
    {
        return Champion::create($data);
    }

    public function destroy(Champion $champion): ?bool
    {
        return $champion->delete();
    }

}
