<?php

namespace App\Repositories;

use App\Models\Slider;
use Illuminate\Database\Eloquent\Collection;

class SliderRepository
{
    public function all(): Collection
    {
        return Slider::all();
    }

    public function store(array $data) {
        return Slider::create($data);
    }

    public function update(array $data, Slider $slider): bool
    {
        return $slider->update($data);
    }

    public function destroy(Slider $slider): ?bool
    {
        return $slider->delete();
    }

    public function paginate(int $items_on_page) {
        return Slider::paginate($items_on_page);
    }
}
