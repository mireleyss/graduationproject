<?php

namespace App\Repositories;

use App\Models\Article;
use Illuminate\Database\Eloquent\Collection;

class ArticleRepository
{
    public function all(): Collection
    {
        return Article::all();
    }

    public function getFirstThreeArticles()
    {
        return Article::orderBy('publication_date', 'DESC')->take(3)->get();
    }

    public function getRecommendedArticles(int $id) {
        return Article::orderBy('publication_date', 'DESC')->where('id', '!=', $id)->get();
    }

    public function getRequiredColumns(array $columns) {
        return Article::select($columns);
    }

    public function paginate(int $items_on_page) {
        return Article::orderBy('publication_date', 'DESC')->paginate($items_on_page);
    }

    public function update(array $data, Article $article): bool
    {
        return $article->update($data);
    }

    public function store(array $data)
    {
        return Article::create($data);
    }

    public function destroy(Article $article): ?bool
    {
        return $article->delete();
    }
}
