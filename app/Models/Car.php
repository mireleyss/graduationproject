<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'max_speed',
        'horsepower',
        'engine_capacity',
        'release_year',
    ];

    public function champions() {
        return $this->belongsTo('champions');
    }
}
