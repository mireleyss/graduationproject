<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Champion extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'info',
        'champion_portrait',
        'car_id',
    ];

    public function car() {
        return $this->hasOne(Car::class, 'id', 'car_id');
    }
}
