<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('cars',function(Blueprint$table){
            $table->id();
            $table->smallInteger('max_speed')->nullable();
            $table->smallInteger('horsepower')->nullable();
            $table->decimal('engine_capacity',2,1)->nullable();
            $table->smallInteger('release_year')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('cars');
    }
};
