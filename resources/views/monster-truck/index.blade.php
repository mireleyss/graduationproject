<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
{{--    <link rel="stylesheet" type="text/css" href="{{ asset('css/zeroing.css') }}">--}}
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.7.1/dist/jquery.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"></script>
    <title>Document</title>
</head>
<body>
<div class="container">
    @include('/components/header')

        <div id="demo" class="carousel slide" data-ride="carousel">

            <!-- Indicators -->
            <ul class="carousel-indicators">
                @for($i = 0; $i < $slider->count(); $i++)
                    <li data-target="#demo" data-slide-to="{{ $i }}" class="{{ ($i == 0) ? 'active' : '' }}"></li>
                @endfor
            </ul>

            <!-- The slideshow -->
            <div class="carousel-inner">
                @foreach($slider as $slide)
                    <div class="carousel-item {{ ($loop->first) ? 'active' : '' }}">
                        <img src="{{ asset('/slider/'.$slide->image) }}" alt="Los Angeles">
                    </div>
                @endforeach
            </div>

            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev">
                <span class="carousel-control-prev-icon"></span>
            </a>
            <a class="carousel-control-next" href="#demo" data-slide="next">
                <span class="carousel-control-next-icon"></span>
            </a>

        </div>



        <div class="content_wrapper">
            <h1><a href="{{ route('articles.index') }}">Новости</a></h1>
            <div class="news_wrapper">
                {{--        {{ isset($articles) ? $articles : '' }}--}}
                @foreach($articles as $article)
                    <div class="news_item">
                        <a href="{{ route('articles.show', $article) }}">
                            @if((!$article->article_image) || (!\Illuminate\Support\Facades\Storage::exists('articles_images/'.$article->article_image)))
                                <img src="{{ asset('/media/not_found.png') }}" alt="article image" class="news_banner" />
                            @else
                                <img src="{{ asset('/articles/'.$article->article_image) }}" alt="{{ $article->header }}" class="news_banner" />
                            @endif
                            <h3>{{ $article->header }}</h3>
                        </a>
                        {{--                    <a href="{{ route('articles.show', $article) }}">--}}
                        {{--                        <img src="{{ asset($article->article_image) }}" alt="article image" class="news_banner">--}}
                        {{--                        <p>{{ $article->header }}</p>--}}
                        {{--                    </a>--}}
                    </div>
                @endforeach
            </div>
            <h1><a href="{{ route('champions.index') }}">Чемпионы</a></h1>
            <div class="champions_wrapper">
                @foreach($champions as $champion)
                    <div class="champions_item">
                        <a href="{{ route('champions.show', $champion) }}">
                            @if((!$champion->champion_portrait) || (!\Illuminate\Support\Facades\Storage::exists('champions_portraits/'.$champion->champion_portrait)))
                                <img src="{{ asset('/media/not_found.png') }}" alt="not found" class="champion_banner" />
                            @else
                                <img src="{{ asset('/champions/'.$champion->champion_portrait) }}" alt="{{ $champion->name.'_portrait' }}" class="champion_banner" />
                            @endif
                            <h3>{{ $champion->name }}</h3>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>

</body>
</html>
