<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/zeroing.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
    <title>Document</title>
</head>
<body>
<div class="container">
    <div class="auth_wrapper">
        <div class="auth_form">
            <div class="auth_nav">
                <a href="{{ URL::previous() }}" class="link">Go Back</a>
                <a href="{{ route('monster-truck.index') }}" class="link">
                    <img src="{{ asset('media/close.png') }}" class="cross-close-btn"/>
                </a>
            </div>
            <form action="{{ route('user.store') }}" method="POST" class="auth_form_inner">
                @csrf
                <h1>Register</h1>
                <div class="form_input_text">
                    <input type="text" placeholder="Name" id="username" class="form-control" name="username"
                           required autofocus>
                    @if ($errors->has('username'))
                        <span class="text-danger">{{ $errors->first('username') }}</span>
                    @endif
                </div>
                <div class="form_input_text">
                    <input type="email" placeholder="Email" id="email" class="form-control"
                           name="email" required autofocus>
                    @if ($errors->has('email'))
                        <span class="text-danger">{{ $errors->first('email') }}</span>
                    @endif
                </div>
                <div class="form_input_text">
                    <input type="password" placeholder="Password" id="password" class="form-control"
                           name="password" required>
                    @if ($errors->has('password'))
                        <span class="text-danger">{{ $errors->first('password') }}</span>
                    @endif
                </div>
                <button type="submit" class="btn btn-register">Register</button>
            </form>
            <div class="hr"></div>
            <a href="{{ route('login') }}" class="link login-link">Already have an account?</a>
        </div>
    </div>
</div>

</body>
</html>
