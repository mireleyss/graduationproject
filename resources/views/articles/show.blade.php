<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<div class="container">
    @include('/components/header')
    <div class="content_wrapper">
        <a href="{{ URL::previous() }}">Go Back</a>
        <div class="champion_wrapper champion_info_wrapper">
            <div>
                @if(($article->article_image) && (\Illuminate\Support\Facades\Storage::exists('articles_images/'.$article->article_image)))
                    <img src="{{ asset('articles/'.$article->article_image) }}" alt="Can't access image" class="champion_banner" />
                @endif
            </div>

            <div class="champion_info">
                <h1>{{ $article->header }}</h1>
                <div class="champion_bio">
                    {!!  $article->content !!}
                </div>
            </div>
        </div>

        <div class="recommended">
            <h1>Читайте далее:</h1>
            <ul>
                @foreach($recommended_articles as $recommended)
                    <li><h3><a href="{{ route('articles.show', $recommended) }}">{{ $recommended->header }}</a></h3></li>
                @endforeach
            </ul>
        </div>
{{--        <div class="champion_wrapper champion_car_wrapper"><h1>{{ $article->header }}</h1></div>--}}




{{--        <a href="{{ URL::previous() }}">Go Back</a>--}}
{{--        <h1>{{ $article->header }}</h1>--}}
{{--        @if(($article->article_image) && (!\Illuminate\Support\Facades\Storage::exists('articles/'.$article->article_image)))--}}
{{--            <img src="{{ asset('/articles/'.$article->article_image) }}" alt="{{ $article->header }}" class="article_image" />--}}
{{--        @endif--}}
{{--        <p>{{ $article->content }}</p>--}}
    </div>
</div>
</body>
</html>
