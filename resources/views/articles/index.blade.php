<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/zeroing.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css">
    <title>Document</title>
</head>
<body>
<div class="container">
    @include('/components/header')
    <div class="content_wrapper">
        <h1>Новости</h1>
        <div class="news_wrapper">
            @foreach($articles as $article)
                <div class="news_item">
                    <a href="{{ route('articles.show', $article) }}">
                        @if(($article->article_image) && (\Illuminate\Support\Facades\Storage::exists('articles_images/'.$article->article_image)))
                            <img src="{{ asset('/articles/'.$article->article_image) }}" alt="{{ $article->header }}" class="news_banner" />
                        @else
                            <img src="{{ asset('/media/not_found.png') }}" alt="article image" class="news_banner" />
                        @endif
                        <h3>{{ $article->header }}</h3>
                    </a>
                </div>
            @endforeach
        </div>
        {{ $articles->links('pagination.default') }}
    </div>
</div>

</body>
</html>
