<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/zeroing.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css">
    <title>Document</title>
</head>
<body>
<div class="container">
    @include('/components/header')
    <div class="content_wrapper">
        <h1>Чемпионы</h1>
        <div class="champions_wrapper">
            @foreach($champions as $champion)
                <div class="champions_item">
                    <a href="{{ route('champions.show', $champion) }}">
                        @if(($champion->champion_portrait) && (\Illuminate\Support\Facades\Storage::exists('champions_portraits/'.$champion->champion_portrait)))
                            <img src="{{ asset('/champions/'.$champion->champion_portrait) }}" alt="{{ $champion->name.'_portrait' }}" class="champion_banner" />
                        @else
                            <img src="{{ asset('/media/not_found.png') }}" alt="not found" class="champion_banner" />
                        @endif
                        <h3>{{ $champion->name }}</h3>
                    </a>
                </div>
            @endforeach
        </div>
        {{ $champions->links('pagination.default') }}
    </div>
</div>

</body>
</html>
