<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div class="container">
        @include('/components/header')
        <div class="content_wrapper">
            <a href="{{ URL::previous() }}">Go Back</a>

            <div class="champion_wrapper champion_info_wrapper">
                @if(($champion->champion_portrait) && (\Illuminate\Support\Facades\Storage::exists('champions_portraits/'.$champion->champion_portrait)))
                    <img src="{{ asset('/champions/'.$champion->champion_portrait) }}" alt="{{ $champion->name.'_portrait' }}" class="champion_banner" />
                @else
                    <img src="{{ asset('/media/not_found.png') }}" alt="not found" class="champion_banner" />
                @endif

                <div class="champion_info">
                    <h1>{{ $champion->name }}</h1>
                    <div class="champion_bio">
                        {!!  $champion->info !!}
                    </div>
                </div>
            </div>
            @if (isset($car_characteristics))
                <div class="champion_wrapper champion_car_wrapper">
                    <h1>Характеристики авто</h1>
                    <div class="champion_car_info">
                        @foreach($car_characteristics->toArray() as $key=>$value)
                            @if(isset($value))
                                <div>
                                    <h1>{{ $value}}</h1>
                                    <h4>{{ $columns[$key] }}</h4>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            @endif
        </div>
        </div>

</body>
</html>
