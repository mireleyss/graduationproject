<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<div class="container">
    @include('/components/header')
    <div class="content_wrapper">
        <h1>Профиль</h1>
        @foreach($user_data as $field)
            <p>{{ $field }}</p>
        @endforeach
        <form method="POST" action="{{ route('user.logout') }}">
            @csrf
            <div class="d-grid mx-auto">
                <button type="submit" class="btn btn-dark btn-block">Logout</button>
            </div>
        </form>
    </div>
</div>
</body>
</html>
