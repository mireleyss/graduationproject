<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div>
        <a href="{{ route('admin.slider') }}">Слайдер</a>
        <a href="{{ route('admin.articles_list') }}">Новости</a>
        <a href="{{ route('admin.champions_list') }}">Чемпионы</a>
        <a href="{{ route('admin.users_list') }}">Пользователи</a>
        <a href="{{ route('admin.cars_list') }}">Автомобили</a>
    </div>
</body>
