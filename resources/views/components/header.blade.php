<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/zeroing.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&display=swap" rel="stylesheet">
{{--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">--}}
{{--    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css">--}}
{{--    <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">--}}
    <title>Document</title>
</head>
<body>
    <header>
        <div class="header">
            <a href="{{ route('monster-truck.index') }}">
                <img src="{{ asset('/media/monster-jam.png') }}" alt="Logo" />
            </a>
            <nav class="header_nav">
                <ul>
                    <li><h3><a href="{{ route('articles.index') }}">News</a></h3></li>
                    <li><h3><a href="{{ route('champions.index') }}">Champions</a></h3></li>
                </ul>
            </nav>
            <div class="user_buttons">
                @if(Auth::check())
                    <a href="{{ route('user.profile') }}">
                        <p>Hello, {{ Auth::user()->username }}</p>
                    </a>
                    @if(Auth::user()->isAdmin)
                        <a href="{{ route('admin_panel') }}">Admin panel</a>
                    @endif
                @else
                    <a href="{{ route('login') }}">
                        <p>Login</p>
                    </a>
                @endif
            </div>
        </div>
    </header>


</body>
</html>
