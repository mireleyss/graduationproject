<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div class="container">
        @include('/components/header')
        @include('/components/admin_navigation')
        <a href="{{ route('admin.create.cars') }}">Добавить</a>
        <table class="information_tb">
            <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Max speed</th>
                <th>Horsepower</th>
                <th>Engine capacity</th>
                <th>Release year</th>
                {{--                <th></th>--}}
                {{--                <th></th>--}}
            </tr>
            </thead>
            @foreach($cars as $car)
                <tr>
{{--                    @foreach($car->toArray() as $key=>$value)--}}
{{--                        @if(!$value)--}}
{{--                            <td>N/A</td>--}}
{{--                        @else--}}
{{--                            <td>{{ $value }}</td>--}}
{{--                        @endif--}}
{{--                    @endforeach--}}
                    <td>{{ $car->id }}</td>
                    <td>{{ $car->name }}</td>
                    <td>{{ $car->max_speed }}</td>
                    <td>{{ $car->horsepower }}</td>
                    <td>{{ $car->engine_capacity }}</td>
                    <td>{{ $car->release_year }}</td>
                    <td><a href="{{route('admin.edit.cars', $car)}}">Изменить</a></td>
                    <td>
                        <form action="{{ route('admin.destroyCars', $car) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" onclick="return confirm('Are you sure you want to delete this car? {{ $car->name }}')">
                                Delete
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach

            {{--        <tr>--}}
            {{--            <td>{{ $champion->id }}</td>--}}
            {{--            <td>{{ $champion->name }}</td>--}}
            {{--            <td>{{ $champion->info }}</td>--}}
            {{--            --}}{{--            <td>{{ $champion->portrait }}</td>--}}
            {{--            --}}{{--            ссылочка на машинку--}}
            {{--            @if(!$champion->car_id)--}}
            {{--                <td>N/A</td>--}}
            {{--            @else--}}
            {{--                <td>{{ $champion->car_id }}</td>--}}
            {{--            @endif--}}
            {{--            <td><a>Изменить</a></td>--}}
            {{--            <td><a>Удалить</a></td>--}}
            {{--        </tr>--}}
        </table>
    </div>

</body>
</html>
