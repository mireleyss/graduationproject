<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css">
    <title>Document</title>
</head>
<body>

<div class="container">
    @include('/components/header')
    @include('/components/admin_navigation')
    <a href="{{ route('admin.create.create_champions') }}">Добавить</a>
    <table class="information_tb">
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Info</th>
                <th>Portrait</th>
                <th>Car</th>
            </tr>
        </thead>
        @foreach($champions as $champion)
            <tr>
                <td>{{ $champion->id }}</td>
                <td><a href="{{ route('champions.show', $champion) }}"><h3>{{ $champion->name }}</h3></a></td>
                <td>{!! $champion->info !!}</td>
                <td>
                    <div class="champions_item">
                        @if(($champion->champion_portrait) && (\Illuminate\Support\Facades\Storage::exists('champions_portraits/'.$champion->champion_portrait)))
                            <img src="{{ asset('/champions/'.$champion->champion_portrait) }}" alt="{{ $champion->name.'_portrait' }}" class="champion_banner" />
                        @else
                            <img src="{{ asset('/media/not_found.png') }}" alt="not found" class="champion_banner" />
                        @endif
                    </div>
                </td>

                @if(!$champion->car_id)
                    <td>N/A</td>
                @else
                    <td>{{ $champion->car->name }}</td>
                @endif

                <td><a href="{{route('admin.edit.champions', $champion)}}">Изменить</a></td>
                <td>
                    <form action="{{ route('admin.destroyChampions', $champion) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit" onclick="return confirm('Are you sure you want to delete this champion? {{ $champion->name }}')">
                            Delete
                        </button>
                    </form>
                </td>
            </tr>
        @endforeach
        {{ $champions->links('pagination.default') }}
    </table>
</div>

</body>
</html>


{{--                @foreach($champion->toArray() as $key=>$value)--}}
{{--                    @if(!$value)--}}
{{--                        <td>N/A</td>--}}
{{--                    @elseif($key == 'car_id')--}}
{{--                        <td>{{$champion->car->name}}</td>--}}
{{--                    @elseif($key == 'champion_portrait')--}}
{{--                        <td>--}}
{{--                            <div class="champions_item">--}}
{{--                                @if(($champion->champion_portrait) && (\Illuminate\Support\Facades\Storage::exists('champions_portraits/'.$champion->champion_portrait)))--}}
{{--                                    <img src="{{ asset('/champions/'.$champion->champion_portrait) }}" alt="{{ $champion->name.'_portrait' }}" class="champion_banner" />--}}
{{--                                @else--}}
{{--                                    <img src="{{ asset('/media/not_found.png') }}" alt="not found" class="champion_banner" />--}}
{{--                                @endif--}}
{{--                            </div>--}}
{{--                        </td>--}}
{{--                    @elseif($key == 'info')--}}
{{--                        <td>{!! $value !!}</td>--}}
{{--                    @else--}}
{{--                        <td>{{ $value }}</td>--}}
