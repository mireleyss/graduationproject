<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div class="container">
        @include('/components/header')
        <div class="content_wrapper">
            <h1>Добавить машину</h1>
            <form method="POST" action="{{ route('admin.store_cars') }}">
                @csrf
                <input type="text"  value="{{ old('name') }}" placeholder="Название машины" id="name" name="name" class="admin_input">
                @if ($errors->has('name'))
                    <span>{{ $errors->first('name') }}</span>
                @endif

                <input type="number" value="{{ old('max_speed') }}" placeholder="Максимальная скорость" min="1" max="300" step="1" id="max_speed" name="max_speed" class="admin_input">
                @if ($errors->has('max_speed'))
                    <span>{{ $errors->first('max_speed') }}</span>
                @endif

                <input type="number" value="{{ old('horsepower') }}" placeholder="Количество лошадиных сил" min="1" max="5000" step="1" id="horsepower" name="horsepower" class="admin_input">
                @if ($errors->has('horsepower'))
                    <span>{{ $errors->first('horsepower') }}</span>
                @endif

                <input type="number" value="{{ old('engine_capacity') }}" placeholder="Объем двигателя" min="1.0" step="0.1" id="engine_capacity" name="engine_capacity" class="admin_input">
                @if ($errors->has('engine_capacity'))
                    <span>{{ $errors->first('engine_capacity') }}</span>
                @endif

                <input type="number" value="{{ old('release_year') }}" placeholder="Дата выпуска" min="1900" max="2099" step="1" id="release_year" name="release_year" class="admin_input">
                @if ($errors->has('release_year'))
                    <span>{{ $errors->first('release_year') }}</span>
                @endif

                <input type="submit">
            </form>
        </div>
    </div>
</body>
</html>
