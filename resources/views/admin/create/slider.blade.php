<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div class="container">
        @include('/components/header')
        <div class="content_wrapper">
            <h1>Добавить слайд</h1>
            <form method="POST" action="{{ route('admin.store_slides') }}" enctype="multipart/form-data">
                @csrf
                <p>Max size: 2MB, acceptable types: png, jpg, jpeg</p>
                <input type="file" id="image" name="image">
                @if ($errors->has('image'))
                    <span>{{ $errors->first('image') }}</span>
                @endif

                <input type="submit">
            </form>
        </div>
    </div>
</body>
</html>
