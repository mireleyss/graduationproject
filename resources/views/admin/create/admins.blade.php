<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<div class="container">
    @include('/components/header')
    <div class="content_wrapper">
        <h1>Добавить админа</h1>
        <form action="{{ route('admin.store_admins') }}" method="POST">
            @csrf
            <input type="text" value="{{ old('username') }}" placeholder="Name" id="username" name="username" class="admin_input"
                   required autofocus>
            @if ($errors->has('username'))
                <span class="text-danger">{{ $errors->first('username') }}</span>
            @endif
            <input type="email" value="{{ old('email') }}" placeholder="Email" id="email"  class="admin_input"
                   name="email" required autofocus>
            @if ($errors->has('email'))
                <span class="text-danger">{{ $errors->first('email') }}</span>
            @endif
            <input type="password" placeholder="Password" id="password"  class="admin_input"
                   name="password" required>
            @if ($errors->has('password'))
                <span class="text-danger">{{ $errors->first('password') }}</span>
            @endif
            <input type="submit">
        </form>
    </div>
</div>
</body>
</html>
