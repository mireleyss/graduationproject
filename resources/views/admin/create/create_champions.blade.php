<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <title>Document</title>
</head>
    <body>
        <div class="container">
            @include('/components/header')
            <div class="content_wrapper">
                <h1>Добавить чемпиона</h1>
                <form method="POST" action="{{ route('admin.store_champions') }}" enctype="multipart/form-data">
                    @csrf
                    <input type="text" value="{{ old('name') }}" placeholder="Имя чемпиона" id="name" name="name" required class="admin_input">
                    @if ($errors->has('name'))
                        <span>{{ $errors->first('name') }}</span>
                    @endif
                    {{--            <textarea class="ckeditor form-control" placeholder="Информация о чемпионе" id="info" name="ckeditor" required></textarea>--}}
                    <textarea class="ckeditor form-control" id="info" name="info">{{ old('info') }}</textarea>
                    @if ($errors->has('info'))
                        <span>{{ $errors->first('info') }}</span>
                    @endif

                    <p>Max size: 2MB, acceptable types: png, jpg, jpeg</p>
                    <input type="file" id="champion_portrait" name="champion_portrait">
                    @if ($errors->has('champion_portrait'))
                        <span>{{ $errors->first('champion_portrait') }}</span>
                    @endif

                    <select id="car_id" name="car_id" class="admin_input">
                        <option value="">--Select car--</option>
                        @foreach($cars as $car)
                            <option value="{{$car->id}}" @selected(old('car_id') == $car->id)>{{$car->name}}</option>
                        @endforeach
                    </select>

                    <input type="submit">
                </form>
            </div>
        </div>

{{--        <div class="container">--}}
{{--            <div class="row">--}}
{{--                <div class="col-md-7 offset-3 mt-4">--}}
{{--                    <div class="card-body">--}}
{{--                        <form method="post" action="" enctype="multipart/form-data">--}}
{{--                            @csrf--}}
{{--                            <div class="form-group">--}}
{{--                                <textarea class="ckeditor form-control" id="ckeditor" name="ckeditor"></textarea>--}}
{{--                            </div>--}}
{{--                        </form>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
        <script src="https://cdn.ckeditor.com/ckeditor5/40.1.0/classic/ckeditor.js"></script>
        <script>
            ClassicEditor.create(document.getElementById("info"), {
                // https://ckeditor.com/docs/ckeditor5/latest/features/toolbar/toolbar.html#extended-toolbar-configuration-format
                toolbar: {
                    items: [ 'paragraph', 'bold', 'italic', '|', 'link', '|', 'undo', 'redo', '|', 'numberedList', 'bulletedList' ]
                }

            });
        </script>
    </body>
</html>
