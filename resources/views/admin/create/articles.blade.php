<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div class="container">
        @include('/components/header')
        <div class="content_wrapper">
            <h1>Добавить новость</h1>
            <form method="POST" action="{{ route('admin.store_articles') }}" enctype="multipart/form-data" class="">
                @csrf
                <input type="text" value="{{ old('header') }}" placeholder="Заголовок новости" id="header" name="header" required class="admin_input">
                @if ($errors->has('header'))
                    <span>{{ $errors->first('header') }}</span>
                @endif

                <textarea class="ckeditor form-control" id="content" name="content">{{ old('content') }}</textarea>
                @if ($errors->has('content'))
                    <span>{{ $errors->first('content') }}</span>
                @endif

                <p>Max size: 2MB, acceptable types: png, jpg, jpeg</p>
                <input type="file" id="article_image" name="article_image">
                @if ($errors->has('article_image'))
                    <span>{{ $errors->first('article_image') }}</span>
                @endif

                <input type="submit">
            </form>
        </div>
    </div>
<script src="https://cdn.ckeditor.com/ckeditor5/40.1.0/classic/ckeditor.js"></script>
<script>
    ClassicEditor.create(document.getElementById("content"), {
        // https://ckeditor.com/docs/ckeditor5/latest/features/toolbar/toolbar.html#extended-toolbar-configuration-format
        toolbar: {
            items: [ 'paragraph', 'bold', 'italic', '|', 'link', '|', 'undo', 'redo', '|', 'numberedList', 'bulletedList' ]
        }

    });
</script>
</body>
</html>
