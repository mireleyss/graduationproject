<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
    <title>Document</title>
</head>
<body>

<div class="container">
    @include('/components/header')
    @include('/components/admin_navigation')
    <a href="{{ route('admin.create.admins') }}">Добавить</a>
    <table class="information_tb">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
        </tr>
        </thead>
        @foreach($users as $user)
            <tr>
                <td>{{ $user->id }}</td>
                <td>{{ $user->username }}</td>
                <td>{{ $user->email }}</td>

                <td>
                    <form action="{{ route('admin.destroyUsers', $user) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit" onclick="return confirm('Are you sure you want to delete this user? {{ $user->email }}')">
                            Delete
                        </button>
                    </form>
                </td>
            </tr>
        @endforeach
        {{ $users->links('pagination.default') }}
    </table>
</div>

</body>
</html>
