<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
    <title>Document</title>
</head>
<body>

<div class="container">
    @include('/components/header')
    @include('/components/admin_navigation')
    <a href="{{ route('admin.create.slider') }}">Добавить</a>
    <table class="information_tb">
        <thead>
        <tr>
            <th>Slide</th>
        </tr>
        </thead>
        @foreach($sliders as $slider)
            <tr>
                <td class="admin_slider_image_wrapper"><img src="{{ asset('slider/'.$slider->image) }}" class="admin_slider_image"/></td>
{{--                <td><p>{{ $slider->image }}</p></td>--}}

                <td><a href="{{route('admin.edit.slides', $slider)}}">Изменить</a></td>
                <td>
                    <form action="{{ route('admin.destroySlides', $slider) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit" onclick="return confirm('Are you sure you want to delete this slide?')">
                            Delete
                        </button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>
</div>

</body>
</html>
