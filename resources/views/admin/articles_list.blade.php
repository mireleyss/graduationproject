<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css">
    <title>Document</title>
</head>
<body>
<div class="container">
    @include('/components/header')
    @include('/components/admin_navigation')
    <a href="{{ route('admin.create.articles') }}">Добавить</a>
    <table class="information_tb">
        <thead>
        <tr>
            <th>ID</th>
            <th>Header</th>
            <th>Content</th>
            <th>Image</th>
            <th>Publication Date</th>
        </tr>
        </thead>
        @foreach($articles as $article)
            <tr>
                <td>{{ $article->id }}</td>
                <td><h3><a href="{{ route('articles.show', $article) }}">{{ $article->header }}</a></h3></td>
                <td>{!! $article->content !!}</td>
                <td>
                    <div class="champions_item">
                        @if(($article->article_image) && (\Illuminate\Support\Facades\Storage::exists('articles_images/'.$article->article_image)))
                            <img src="{{ asset('articles/'.$article->article_image) }}" alt="Can't access image" class="champion_banner" />
                        @else
                            <p>No image</p>
                        @endif
                    </div>
                </td>
                <td>{{ $article->publication_date }}</td>
                <td><a href="{{route('admin.edit.articles', $article)}}">Изменить</a></td>
                <td>
                    <form action="{{ route('admin.destroyArticles', $article) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit" onclick="return confirm('Are you sure you want to delete this article? {{ $article->header }}')">
                            Delete
                        </button>
                    </form>
                </td>
            </tr>
        @endforeach
        {{ $articles->links('pagination.default') }}
    </table>
</div>
</body>
</html>




{{--                @foreach($article->toArray() as $key=>$value)--}}
{{--                    @if($key == 'article_image')--}}
{{--                        <td>--}}
{{--                            <div class="champions_item">--}}
{{--                                мб сделать наоборот проверку на существование и если нет то no image--}}
{{--                                @if(($article->article_image) && (\Illuminate\Support\Facades\Storage::exists('articles_images/'.$article->article_image)))--}}
{{--                                    <img src="{{ asset('articles/'.$article->article_image) }}" alt="Can't access image" class="champion_banner" />--}}
{{--                                @else--}}
{{--                                    <p>No image</p>--}}
{{--                                @endif--}}
{{--                            </div>--}}
{{--                        </td>--}}
{{--                    @else--}}
{{--                        <td>{{ $value }}</td>--}}
{{--                    @endif--}}
{{--                @endforeach--}}
