<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div class="container">
        @include('/components/header')
        <form method="POST" action="{{ route('user.logout') }}">
            @csrf
            <div class="d-grid mx-auto">
                <button type="submit" class="btn btn-dark btn-block">Logout</button>
            </div>
        </form>
        @include('/components/admin_navigation')
    </div>
{{--<div>--}}
{{--    <a>Баннер</a>--}}
{{--    <a>Новости</a>--}}
{{--    <a href="{{ route('admin.champions_list') }}">Чемпионы</a>--}}
{{--    <a>Пользователи</a>--}}
{{--    <a href="{{ route('admin.cars_list') }}">Автомобили</a>--}}
{{--</div>--}}
</body>
</html>
