<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<div class="container">
    @include('/components/header')
    <div class="content_wrapper">
        <h1>Изменить данные чемпиона {{ $champion->name }}</h1>
        <form method="POST" action="{{ route('admin.updateChampions', $champion) }}" enctype="multipart/form-data">
            @csrf
            @method('PUT')

            <input type="text" value="{{ old('name', $champion->name) }}" placeholder="Имя чемпиона" id="name" name="name" required class="admin_input">
            @if ($errors->has('name'))
                <span>{{ $errors->first('name') }}</span>
            @endif

            <textarea class="ckeditor form-control" id="info" name="info">{{ old('info', $champion->info) }}</textarea>
            @if ($errors->has('info'))
                <span>{{ $errors->first('info') }}</span>
            @endif

            <p>Max size: 2MB, acceptable types: png, jpg, jpeg</p>
            <input type="file" id="champion_portrait" name="champion_portrait">

            <select id="car_id" name="car_id" class="admin_input">
                <option value="">--Select car--</option>

                @foreach($cars as $car)
                    <option value="{{$car->id}}" @selected(old('car_id') == $car->id)>{{$car->name}}</option>
                @endforeach
            </select>
            <input type="submit">
        </form>
    </div>
</div>
<script src="https://cdn.ckeditor.com/ckeditor5/40.1.0/classic/ckeditor.js"></script>
<script>
    ClassicEditor.create(document.getElementById("info"), {
        // https://ckeditor.com/docs/ckeditor5/latest/features/toolbar/toolbar.html#extended-toolbar-configuration-format
        toolbar: {
            items: [ 'paragraph', 'bold', 'italic', '|', 'link', '|', 'undo', 'redo', '|', 'numberedList', 'bulletedList' ]
        }

    });
</script>
</body>
</html>
